FROM opensuse/leap:latest

LABEL author="Tobias Hofmann"
LABEL website="https://www.itsfullofstars.de"

RUN zypper update && zypper --non-interactive install --replacefiles wget unzip glibc-locale which java-11-openjdk && zypper clean

ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 

RUN wget -O fscrawler.zip https://oss.sonatype.org/content/repositories/snapshots/fr/pilato/elasticsearch/crawler/fscrawler-es7/2.7-SNAPSHOT/fscrawler-es7-2.7-20200702.105129-114.zip

RUN unzip fscrawler.zip

RUN mv *SNAPSHOT ./fscrawler/

RUN mkdir /etc/fscrawler
RUN mkdir -p /var/lib/fscrawler/data

WORKDIR fscrawler

CMD ["./bin/fscrawler", "--config_dir", "/etc/fscrawler", "documents"]