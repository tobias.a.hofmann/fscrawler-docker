# Introduction

In this project you can find a Docker image for fscrawler ([documentation](https://fscrawler.readthedocs.io/en/latest/) and [repo on GitHub](https://github.com/dadoonet/fscrawler)). The Docker image is using [fscrawler 2.7](https://oss.sonatype.org/content/repositories/snapshots/fr/pilato/elasticsearch/crawler/fscrawler-es7/2.7-SNAPSHOT/). In case you do not want to build your own Docker image, you can download one from [Docker hub](https://hub.docker.com/repository/docker/tobiashofmann/fscrawler-docker).

Besides the Dockerfile needed to create the image, you can find a short description on how to run fscrawler using the image.

```sh
docker pull tobiashofmann/fscrawler-docker:latest
```

# Landscape

To be able to run the fscrawler Docker image, you need a running ElasticSearch server.

## Network

Create a Docker network named elastic.

```sh
docker network create elastic
```

## Elastic

See Elastic documentation on how to run elstic inside a docker container. [ElasticSearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html).

Get the image

```sh
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.8.0
```

Run container. The following command will start an Elastic container named elastic (also the DNS name) and attach it to the network elastic. The server will listen to port 9200 and be in single node mode.

```sh
docker run -p 9200:9200 -e "discovery.type=single-node" --net elastic --name elastic docker.elastic.co/elasticsearch/elasticsearch:7.8.0
```

You can access the server via your browser by opening http://localhost:9200

**Note**

Make sure that your Docker daemon has enough memory available. The default 2 GB RAM assigned to it are not enough for ElasticSearch. Increase it to e.g. 5 GB. If the memory is too low, when FSCrawler starts to index files, ES may crash with an error message like _"Native controller process has stopped - no new native processes can be started"_.

## FS Crawler

Start fscrawler in a container and bind it to the same network as Elastic Search. This will allow fscrawler to connect to port 9200 of your Elastic server.

You'll have to provide two volumes / file shares:

1. Data volume. Contains the files  to be index.
2. Config file for fscrawler.

Your config file must match the fscrawler container layout. OCR needs to be disabled, the container image is not configured to support tesseract.

```yaml
url: "/var/lib/fscrawler/data"
ocr:
  - enabled: false
elasticsearch:
  nodes:
  - url: "http://elastic:9200"
```  

```sh  
docker run \
--net elastic \
-d  \
--name fscrawler \
-v /Users/thofmann/Documents/Blog:/var/lib/fscrawler/data:ro \
-v /Users/thofmann/.fscrawler:/etc/fscrawler \
fscrawlerdocker:latest
```

# FSCrawler CLI options

Restart and reindex all documents, independently if they were already indexed.

./bin/fscrawler --config_dir /etc/fscrawler --loop 1 --restart documents